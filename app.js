var express = require('express');
var app = express();
var mysql = require('mysql');
var website_url = "http://localhost:4200";
var bodyParser = require('body-parser')

app.use(function (req, res, next) {
    global.connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : 'SmartWork',
        database : 'ept'
      });
    connection.connect();
    next();
});

app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json());

//Enable cors origin
var cors = require('cors');
app.use(cors({
origin: website_url
}));

const getUser = require('./routes/user');
app.use('/api/user', getUser);




app.listen(3000, () =>
  console.log('Example app listening on port 3000!'),
);

