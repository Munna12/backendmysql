var express = require('express')
const router = express.Router();

console.log("router", router)
// var userLists = [{
//     name: "kumar", age: 18, depart: "developer"
// },
// {
//     name: "munna", age: 17, depart: "developer"
// },
// {
//     name: "praveen", age: 16, depart: "developer"
// }, 
// {
//     name: "mohan", age: 33, depart: "developer"
// }
// ]

router.get('/userList', function (req, res) {
    connection.query('SELECT id, firstname, lastname, username FROM user', (error, data) => {
        if (error) {
            console.log('userlist error', error)
        }
        else {
            console.log('userlist', data)
            res.status(200).json(data);
        }
    })

})

router.post('/update', function (req, res) {
    console.log("req", req)
    connection.query('UPDATE user SET firstname =?, lastname =?, username =? WHERE id=?', [req.body.firstname, req.body.lastname, req.body.username,  req.body.id], (error, data)=> {
        if(error) {
            console.log('Updatedata error', error)
        }
        else{
            console.log('Updatedata', data)
            res.status(200).json(data);
        }
    })
})

router.get('/getuser/:id', function (req, res) {
    console.log("req", req)
    connection.query('SELECT id, firstname, lastname, username FROM user WHERE id =?', [req.params.id], (error, data)=> {
        if(error) {
            console.log('Single User error', error)
        }
        else{
            console.log('Single User', data)
            res.status(200).json(data);
        }
    })
})





module.exports = router;